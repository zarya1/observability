# observability
observability samples
## prometheus
web service 
```
cmd/prom/
└── main.go
```
instruments any HTTP request
```
internal/metrics/
└── prom.go
```
testing...
```
./bin/prom-srv
```
make some calls...
```
curl http://localhost:8080/hash -d "foo"
curl http://localhost:8080/hash -d "bar"
```
verity the metrics
```
curl http://localhost:8080/metrics
```
