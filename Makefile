.PHONY: all
all: build
FORCE: ;

SHELL  := env PROJETO_ENV=$(PROJETO_ENV) $(SHELL)
PROJETO_ENV ?= dev

BIN_DIR = $(PWD)/bin
GIT_COMMIT=$(shell git rev-list -1 HEAD)

.PHONY: build

clean:
	rm -rf bin/*

dependencies:
	go mod download

build: dependencies linux-binaries

linux-binaries:
	CGO_ENABLED=0 GOOS=linux go build -tags "$(PROJECT_ENV)" -ldflags "-X 'main.GitCommit=$(GIT_COMMIT)'" -o $(BIN_DIR)/prom-srv cmd/prom/main.go

